package com.thelsien.projects.cajongroovegenerator.ui.generator.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thelsien.projects.cajongroovegenerator.R
import com.thelsien.projects.cajongroovegenerator.data.room.GrooveEntity

class GrooveListAdapter(
    private val items: ArrayList<GrooveEntity>,
    private val adapterListener: GrooveAdapterListener
) :
    RecyclerView.Adapter<GrooveListAdapter.GrooveViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GrooveViewHolder {
        return GrooveViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_groove_item_view,
                parent,
                false
            ), adapterListener
        )
    }

    override fun onBindViewHolder(holder: GrooveViewHolder, position: Int) {
        holder.grooveView.text = items[position].name
        holder.loadButton.setOnClickListener(holder)
        holder.deleteButton.setOnClickListener(holder)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * Used to update all the items in the adapter.
     *
     * Calls [notifyDataSetChanged].
     *
     * @param newItems the new items being inserted.
     */
    fun setItems(newItems: ArrayList<GrooveEntity>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    /**
     * Adds a new items into the adapter.
     *
     * Calls [notifyItemInserted].
     *
     * @param newItem the new item being inserted.
     */
    fun addItem(newItem: GrooveEntity) {
        items.add(newItem)
        notifyItemInserted(items.size - 1)
    }

    /**
     * Removes an item from the adapter.
     *
     * Calls [notifyDataSetChanged].
     *
     * @param position the position of the item to be removed.
     */
    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    /**
     * Removes an item with the given id.
     *
     * @param removeId the id for the item to be removed.
     */
    fun removeItemWithId(removeId: Int) {
        var position = -1
        for (item in items) {
            position++
            if (item.id == removeId) {
                items.removeAt(position)
                break
            }
        }

        if (position != -1) {
            notifyItemRemoved(position)
        }
    }

    /**
     * The view holder class for the GROOVE list adapter.
     */
    inner class GrooveViewHolder(itemView: View, private val grooveAdapterListener: GrooveAdapterListener) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val grooveView: TextView = itemView.findViewById(R.id.tv_groove_text)
        val loadButton: Button = itemView.findViewById(R.id.btn_load)
        val deleteButton: Button = itemView.findViewById(R.id.btn_delete)

        override fun onClick(v: View?) {
            val grooveId = items[adapterPosition].id
            when (v?.id) {
                R.id.btn_load -> grooveAdapterListener.onGrooveLoadClicked(grooveId)
                R.id.btn_delete -> grooveAdapterListener.onGrooveDeleted(grooveId)
                else -> {
                    // do nothing.
                }
            }
        }
    }
}

/**
 * An interface for [GrooveListAdapter] related events.
 */
interface GrooveAdapterListener {
    fun onGrooveLoadClicked(grooveId: Int?)
    fun onGrooveDeleted(grooveId: Int)
}