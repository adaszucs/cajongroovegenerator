package com.thelsien.projects.cajongroovegenerator.data

import androidx.room.Embedded
import androidx.room.Ignore

/**
 * Describes a single beat from a GROOVE.
 *
 * @param beatType type of the beat. Can be anything from [BeatType].
 */
data class Beat(@Embedded val beatType: BeatType) {

    /**
     * After sound pool loaded a beat, this must be set.
     */
    @Ignore
    var soundId: Int? = null

    /**
     * Describes the length of the beat. [BeatLength.PAUSE_SIXTEENTH] if type is [BeatType.NO_TYPE].
     */
    @Embedded
    var beatLength: BeatLength = if (beatType == BeatType.NO_TYPE) {
        BeatLength.PAUSE_SIXTEENTH
    } else {
        BeatLength.QUARTER
    }
}