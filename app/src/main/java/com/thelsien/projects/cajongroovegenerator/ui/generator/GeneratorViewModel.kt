package com.thelsien.projects.cajongroovegenerator.ui.generator

import android.app.Application
import android.media.SoundPool
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.thelsien.projects.cajongroovegenerator.data.Beat
import com.thelsien.projects.cajongroovegenerator.data.room.GrooveDatabase
import com.thelsien.projects.cajongroovegenerator.data.room.GrooveEntity
import com.thelsien.projects.cajongroovegenerator.data.room.SavedBeatEntity
import com.thelsien.projects.cajongroovegenerator.sound.GroovePlayer
import com.thelsien.projects.cajongroovegenerator.sound.GroovePlayerListener
import com.thelsien.projects.cajongroovegenerator.sound.SoundPoolManager
import com.thelsien.projects.cajongroovegenerator.ui.generator.adapter.GrooveAdapterListener
import com.thelsien.projects.cajongroovegenerator.ui.generator.adapter.GrooveListAdapter
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class GeneratorViewModel(application: Application) : AndroidViewModel(application),
    GrooveAdapterListener, GroovePlayerListener, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    private var job = Job()

    private var loadedGrooveId: Int? = null

    private val grooveDatabase: GrooveDatabase = Room.databaseBuilder(
        getApplication(),
        GrooveDatabase::class.java,
        "groove_db"
    ).build()

    val grooveAdapter: GrooveListAdapter = GrooveListAdapter(ArrayList(), this)

    init {
        // load the adapter data.
        launch {
            val savedGrooves = grooveDatabase.grooveDao().getAllGrooves()
            GlobalScope.launch(Dispatchers.Main) {
                grooveAdapter.setItems(ArrayList(savedGrooves))
            }
        }
    }

    /**
     * Saves a GROOVE for playback reasons.
     */
    fun saveGroove() {
        if (GroovePlayer.generatedBeatArray.value == null) {
            // nothing to do.
            return
        }

        launch {
            val grooveEntity = GrooveEntity("demo name")
            val grooveId = grooveDatabase.grooveDao().insertGroove(grooveEntity).toInt()
            //val grooveId = insertedIds.toInt()
            for (beat in GroovePlayer.generatedBeatArray.value!!) {
                grooveDatabase.grooveDao().insertSavedBeats(SavedBeatEntity(grooveId, beat))
            }
            grooveEntity.id = grooveId
            GlobalScope.launch(Dispatchers.Main) {
                grooveAdapter.addItem(grooveEntity)
            }
        }
    }

    /**
     * Called when the new random GROOVE button is clicked.
     *
     * If the [SoundPool] is not initialized, it will call the initialization process, otherwise it will start playing
     * the GROOVE.
     */
    fun onGenerateButtonClicked() {
        GroovePlayer.startPlayingGroove(getApplication())
    }

    /**
     * Resumes playing the currently generated GROOVE.
     */
    fun resumePlayingSound() {
        GroovePlayer.resumePlaying()
    }

    /**
     * Cancels playing any sound.
     */
    fun cancelPlayingSound() {
        GroovePlayer.cancelPlayingSound()
    }

    /**
     * Returns the generated beat array from the [GroovePlayer].
     */
    fun getGeneratedBeatArray(): MutableLiveData<ArrayList<Beat>> {
        return GroovePlayer.generatedBeatArray
    }

    /**
     * Returns the bpm live data from the [GroovePlayer].
     *
     * @return the bpm live data object.
     */
    fun getBpm(): MutableLiveData<Int> {
        return GroovePlayer.bpm
    }

    override fun onGrooveLoadClicked(grooveId: Int?) {
        loadedGrooveId = grooveId
        if (loadedGrooveId != null) {
            if (!GroovePlayer.isSoundPoolInitialized()) {
                GroovePlayer.initSoundPool(getApplication(), this)
                return
            }
        } else {
            return
        }

        launch {
            val savedBeatsList = grooveDatabase.grooveDao().getSavedBeatsForGroove(loadedGrooveId!!)
            val grooveBeatsList = ArrayList<Beat>(savedBeatsList.size)

            for (savedBeat in savedBeatsList) {
                val beat = Beat(savedBeat.beat.beatType)
                beat.beatLength = savedBeat.beat.beatLength
                beat.soundId = GroovePlayer.getSoundIdForBeatType(beat.beatType)
                grooveBeatsList.add(beat)
            }

            GlobalScope.launch(Dispatchers.Main) {
                GroovePlayer.generatedBeatArray.value = grooveBeatsList
                GroovePlayer.startPlayingGroove(getApplication(), true)
            }
        }
    }

    override fun onGrooveDeleted(grooveId: Int) {
        launch {
            val grooveEntity = grooveDatabase.grooveDao().findGrooveWithId(grooveId)
            grooveDatabase.grooveDao().deleteSavedBeatsForGrooveId(grooveId)
            grooveDatabase.grooveDao().deleteGroove(grooveEntity)

            GlobalScope.launch(Dispatchers.Main) {
                grooveAdapter.removeItemWithId(grooveId)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()

        GroovePlayer.cancelPlayingSound()
        SoundPoolManager.pausePlaying(true)
        job.cancel()
    }

    override fun onAllSoundIdsLoaded() {
        onGrooveLoadClicked(loadedGrooveId)
    }

    /**
     * Decrease the bpm value.
     */
    fun decreaseBpm() {
        if (GroovePlayer.bpm.value!! > 30) {
            GroovePlayer.bpm.value = GroovePlayer.bpm.value?.minus(5)
        }
    }

    /**
     * Increase the bpm value.
     */
    fun increaseBpm() {
        if (GroovePlayer.bpm.value!! < 200) {
            GroovePlayer.bpm.value = GroovePlayer.bpm.value?.plus(5)
        }
    }
}