package com.thelsien.projects.cajongroovegenerator.data

import androidx.room.ColumnInfo
import com.thelsien.projects.cajongroovegenerator.data.BeatLength.Companion.PAUSE_SIXTEENTH

/**
 * A data class describing the interval of a beat.
 *
 * @param lengthId the lengthId for the beat length. Used when generating a GROOVE.
 */
data class BeatLength(@ColumnInfo(name = "length_id") var lengthId: Int) {
    companion object {
        val INVALID_LENGTH = BeatLength(-1)
        val PAUSE_SIXTEENTH = BeatLength(0)
        val QUARTER = BeatLength(1)
        val EIGHT = BeatLength(2)
        val SIXTEENTH = BeatLength(3)

        /**
         * Returns a beat length for the given lengthId parameter.
         *
         * @param beatLengthId an lengthId that we need to return a beat length.
         * @return returns a valid beat length for a valid lengthId. If the lengthId is not in range, it return [PAUSE_SIXTEENTH].
         */
        fun getBeatLengthById(beatLengthId: Int): BeatLength {
            return when (beatLengthId) {
                0 -> PAUSE_SIXTEENTH
                1 -> QUARTER
                2 -> EIGHT
                3 -> SIXTEENTH
                else -> INVALID_LENGTH
            }
        }

        /**
         * Calculates the delay for the beat with the given `bpm` and `beatLength`.
         *
         * @param bpm beats per minute.
         * @param beatLength the length of the beat. One of the values from [BeatLength].
         *
         * @return the delay based on the bpm and beat length provided.
         */
        fun calculateDelayForBeaLength(bpm: Int, beatLength: BeatLength): Float {
            val bpmInMillis = (60 / bpm.toFloat()) * 1000f
            return when (beatLength) {
                BeatLength.QUARTER -> bpmInMillis
                BeatLength.EIGHT -> bpmInMillis / 2f
                BeatLength.SIXTEENTH -> bpmInMillis / 4f
                else -> bpmInMillis / 4f
            }
        }
    }
}
