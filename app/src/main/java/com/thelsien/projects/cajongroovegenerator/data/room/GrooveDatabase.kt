package com.thelsien.projects.cajongroovegenerator.data.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [GrooveEntity::class, SavedBeatEntity::class], version = 1, exportSchema = false)
abstract class GrooveDatabase : RoomDatabase() {
    abstract fun grooveDao(): GrooveDao
}