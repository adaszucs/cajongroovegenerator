package com.thelsien.projects.cajongroovegenerator.data

import androidx.room.ColumnInfo

data class BeatType(@ColumnInfo(name = "type_id") val typeId: Int) {
    companion object {
        val NO_TYPE = BeatType(0)
        val KICK = BeatType(1)
        val SNARE = BeatType(2)

        /**
         * Returns a beat type for the provided `typeId`.
         *
         * @param typeId an lengthId for a beat type.
         * @return returns a beat type from the enum. Returns [NO_TYPE], if there is no associated beat type for the
         * parameter.
         */
        fun getBeatTypeById(typeId: Int): BeatType {
            return when (typeId) {
                1 -> KICK
                2 -> SNARE
                else -> NO_TYPE
            }
        }
    }
}