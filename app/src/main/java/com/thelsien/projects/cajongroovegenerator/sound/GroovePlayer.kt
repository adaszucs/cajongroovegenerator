package com.thelsien.projects.cajongroovegenerator.sound

import android.app.Application
import android.media.SoundPool
import androidx.lifecycle.MutableLiveData
import com.thelsien.projects.cajongroovegenerator.data.Beat
import com.thelsien.projects.cajongroovegenerator.data.BeatLength
import com.thelsien.projects.cajongroovegenerator.data.BeatType
import com.thelsien.projects.cajongroovegenerator.sound.GroovePlayer.playBeat
import com.thelsien.projects.cajongroovegenerator.sound.SoundPoolManager.playBeat
import kotlinx.coroutines.*
import kotlinx.coroutines.NonCancellable.isActive
import java.util.*
import kotlin.coroutines.CoroutineContext

/**
 * Singleton for playing and generating GROOVEs.
 */
object GroovePlayer : CoroutineScope, GroovePlayerListener {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    /**
     * Random generator.
     */
    private val rand: Random = Random()

    /**
     * A coroutine job.
     */
    private var job = Job()

    /**
     * Number of loaded beats. If it reaches [beatItemCount], the GROOVE can be played.
     */
    private var loadedBeats = 0

    /**
     * `true` if sound is playing, `false` otherwise
     */
    private var isPlaying: Boolean = false

    /**
     * The currently played beat's index.
     */
    private var playIndex = 0

    //TODO sound type means it can be a drum or cajon or anything else.
    /**
     * The beat item count for the currently selected sound type.
     */
    var beatItemCount = 0
        private set

    /**
     * The generated GROOVE's beat array
     */
    var generatedBeatArray = MutableLiveData<ArrayList<Beat>>()
        private set

    /**
     * Beats per minute.
     */
    var bpm = MutableLiveData<Int>()

    init {
        bpm.value = 90 //default value
    }

    /**
     * Starts playing of a GROOVE.
     *
     * If a [SoundPool] is not initialized, this method will take care of that.
     *
     * @param application an Android application. Used for loading in the currently selected sound type's beats.
     * @param isLoadedFromList if the GROOVE is loaded from a list the value is `true`. Default value is `false`.
     */
    fun startPlayingGroove(application: Application, isLoadedFromList: Boolean = false) {
        cancelPlayingSound()

        if (!isSoundPoolInitialized()) {
            initSoundPool(application, this)
        } else {
            startPlayingSound(isLoadedFromList)
        }
    }

    /**
     * Stops playing sound.
     */
    fun cancelPlayingSound() {
        isPlaying = false
        SoundPoolManager.pausePlaying()
        job.cancel()
        job = Job()
    }

    /**
     * Initiates the [SoundPool] before actually we start playing beats. This is needed when the generate button
     * clicked the first time or when the sound pool is `null`.
     *
     * @param application an Android application. Used for loading in the currently selected sound type's beats.
     */
    fun initSoundPool(
        application: Application,
        groovePlayerListener: GroovePlayerListener
    ) {
        clearBeatsMap()
        loadedBeats = 0

        SoundPoolManager.initSoundPool(SoundPool.OnLoadCompleteListener { _, _, _ ->
            loadedBeats++
            if (loadedBeats == beatItemCount) {
                groovePlayerListener.onAllSoundIdsLoaded()
            }
        })

        loadBeatsSoundIds(application)
    }

    /**
     * Returns whether the sound pool is initialized or not.
     *
     * @return `true` if initialized, `false` otherwise.
     */
    fun isSoundPoolInitialized(): Boolean {
        return SoundPoolManager.soundPool != null
    }

    /**
     * Resume playing sound where it as left.
     */
    fun resumePlaying() {
        if (isPlaying) {
            return
        }

        isPlaying = true
        playBeat()
    }

    /**
     * Returns a sound id from the beats map for the given beat type.
     *
     * @param beatType a beat type.
     * @return a sound id, loaded by the sound pool. Returns [SoundPoolManager.INVALID_SOUND_ID] if there is no sound
     * id for the beat type.
     */
    fun getSoundIdForBeatType(beatType: BeatType): Int? {
        if (beatType == BeatType.NO_TYPE) {
            return SoundPoolManager.INVALID_SOUND_ID
        }

        val soundIdsForType = SoundPoolManager.beatsMap[beatType.typeId]
        return soundIdsForType!![rand.nextInt(soundIdsForType.size)].soundId
    }

    /**
     * Loads the beat sound ids with a [SoundPool] into the [SoundPoolManager.beatsMap].
     *
     * @param application an Android application.
     */
    private fun loadBeatsSoundIds(application: Application) {
        launch {
            val kickPair = Pair(
                BeatType.KICK,
                getBeatIdsFromRawFolder(
                    application,
                    "kick_"
                )
            )
            val snarePair = Pair(
                BeatType.SNARE,
                getBeatIdsFromRawFolder(
                    application,
                    "snare_"
                )
            )

            SoundPoolManager.loadSoundPoolIdsForBeats(
                application,
                kickPair,
                snarePair
            )
        }
    }

    /**
     * Gets the beat resource ids from the raw folder with the given parameter prefix.
     *
     * @param application an Android application.
     * @param resourcePrefix the prefix for getting a collection of beats.
     * @return the list of resource ids for the given prefix.
     */
    private fun getBeatIdsFromRawFolder(application: Application, resourcePrefix: String): ArrayList<Int> {
        val rawBeatIds = ArrayList<Int>()
        for (i in 0..1000) {
            val rawBeatId = application.resources.getIdentifier(
                resourcePrefix + i,
                "raw",
                application.packageName
            )
            if (rawBeatId == 0) {
                break
            }
            rawBeatIds.add(rawBeatId)
            beatItemCount++
        }
        return rawBeatIds
    }

    /**
     * Generates a random beat index list.
     */
    private fun generateRandomBeats() {
        val newBeatArray = ArrayList<Beat>()
        var previousBeat: Beat? = null
        var maxPauses: Int
        var pauses = 0

        for (i in 1..16) {
            var beat = Beat(BeatType.NO_TYPE)
            if (previousBeat != null &&
                previousBeat.beatLength != BeatLength.SIXTEENTH &&
                previousBeat.beatLength != BeatLength.PAUSE_SIXTEENTH
            ) {
                // adding sixteenth pauses
                maxPauses = when (previousBeat.beatLength) {
                    BeatLength.QUARTER -> 3
                    BeatLength.EIGHT -> 1
                    else -> 0
                }
                if (pauses < maxPauses) {
                    pauses++
                } else {
                    pauses = 0
                    previousBeat = null
                }
            } else {
                val randomBeatType = BeatType.getBeatTypeById(rand.nextInt(2) + 1)
                val randomBeatLength = BeatLength.getBeatLengthById(rand.nextInt(3) + 1)
                val typedBeatArray = SoundPoolManager.beatsMap[randomBeatType.typeId]

                beat = typedBeatArray!![rand.nextInt(typedBeatArray.size)]
                beat.beatLength = randomBeatLength
                previousBeat = beat
            }

            if (i == 16) {
                beat.beatLength = BeatLength.SIXTEENTH
            }

            newBeatArray.add(beat)
        }

        generatedBeatArray.value = newBeatArray
    }

    /**
     * Clears the beat map.
     */
    private fun clearBeatsMap() {
        SoundPoolManager.beatsMap.clear()
    }

    /**
     * Start SOME GROOVING! Setup before actually calling [playBeat].
     *
     * @param isBeatLoaded `true`, if the beat is loaded from the list, otherwise `false`.
     */
    private fun startPlayingSound(isBeatLoaded: Boolean = false) {
        if (!isBeatLoaded) {
            generateRandomBeats()
        }

        playIndex = 0
        isPlaying = true

        playBeat()
    }

    /**
     * Plays a single note of the GROOVE. Calls itself recursively, until [isPlaying] or [isActive] breaks it.
     */
    private fun playBeat() {
        val isGeneratedBeatArrayEmpty = generatedBeatArray.value?.isEmpty() ?: false
        if (generatedBeatArray.value == null || isGeneratedBeatArrayEmpty) {
            //if the generated beat array is empty or null, do nothing
            return
        }

        launch {
            if (isActive && isPlaying) {
                val currentBeat = generatedBeatArray.value?.get(
                    playIndex
                )
                if (currentBeat?.beatType != BeatType.NO_TYPE) {
                    SoundPoolManager.playBeat(currentBeat?.soundId!!)
                }

                if (playIndex < (generatedBeatArray.value?.size?.minus(1))!!) {
                    playIndex += 1
                } else {
                    playIndex = 0
                }

                val delayValue = BeatLength.calculateDelayForBeaLength(bpm.value!!, currentBeat.beatLength)

                delay(delayValue.toLong())

                playBeat()
            }
        }
    }

    override fun onAllSoundIdsLoaded() {
        startPlayingSound()
    }
}

interface GroovePlayerListener {
    fun onAllSoundIdsLoaded()
}