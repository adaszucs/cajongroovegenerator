package com.thelsien.projects.cajongroovegenerator.data.room

import androidx.room.*
import com.thelsien.projects.cajongroovegenerator.data.Beat

@Entity(
    tableName = "saved_beats",
    indices = [Index(value = arrayOf("groove_id"))],
    foreignKeys = [ForeignKey(
        entity = GrooveEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("groove_id")
    )]
)
data class SavedBeatEntity(
    @ColumnInfo(name = "groove_id") var grooveId: Int,
    @Embedded var beat: Beat
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}