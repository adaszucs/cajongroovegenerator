package com.thelsien.projects.cajongroovegenerator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thelsien.projects.cajongroovegenerator.ui.generator.GeneratorFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction()
            .add(android.R.id.content, GeneratorFragment()) //TODO navigation should be used here?
            .commit()
    }
}