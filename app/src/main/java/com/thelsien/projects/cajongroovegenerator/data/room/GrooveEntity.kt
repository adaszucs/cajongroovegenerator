package com.thelsien.projects.cajongroovegenerator.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "grooves")
data class GrooveEntity(
    var name: String
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}