package com.thelsien.projects.cajongroovegenerator.ui.generator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thelsien.projects.cajongroovegenerator.R

class GeneratorFragment : Fragment() {
    private lateinit var viewModel: GeneratorViewModel

    private var currentGeneratedArrayView: TextView? = null
    private var bpmView: TextView? = null

    private lateinit var listView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this)[GeneratorViewModel::class.java]
        viewModel.getGeneratedBeatArray().observe(this, Observer { elements ->
            currentGeneratedArrayView?.text =
                "currently generated ${elements.joinToString { it.beatType.typeId.toString() }}"
        })
        viewModel.getBpm().observe(this, Observer { bpm ->
            bpmView?.text = bpm.toString()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_generator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currentGeneratedArrayView = view.findViewById(R.id.tv_currently_generated)
        bpmView = view.findViewById(R.id.tv_bpm)

        listView = view.findViewById(R.id.rv_list)
        val stopButton: Button = view.findViewById(R.id.btn_stop)
        val generateButton: Button = view.findViewById(R.id.btn_play)
        val resumeButton: Button = view.findViewById(R.id.btn_resume)
        val saveButton: Button = view.findViewById(R.id.btn_save)
        val increaseButton: Button = view.findViewById(R.id.btn_increase)
        val decreaseButton: Button = view.findViewById(R.id.btn_decrease)
        generateButton.setOnClickListener {
            viewModel.onGenerateButtonClicked()
        }

        stopButton.setOnClickListener {
            viewModel.cancelPlayingSound()
        }

        resumeButton.setOnClickListener {
            viewModel.resumePlayingSound()
        }

        saveButton.setOnClickListener {
            viewModel.saveGroove()
        }

        increaseButton.setOnClickListener {
            viewModel.increaseBpm()
        }

        decreaseButton.setOnClickListener {
            viewModel.decreaseBpm()
        }

        setupListView()
    }

    /**
     * Sets up the list of saved GROOVES.
     */
    private fun setupListView() {
        listView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        listView.adapter = viewModel.grooveAdapter
    }

    override fun onPause() {
        super.onPause()

        viewModel.cancelPlayingSound()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel.cancelPlayingSound()
    }
}