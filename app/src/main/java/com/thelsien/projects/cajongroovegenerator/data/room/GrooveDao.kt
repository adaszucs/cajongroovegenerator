package com.thelsien.projects.cajongroovegenerator.data.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface GrooveDao {
    @Query("SELECT * FROM grooves")
    fun getAllGrooves(): List<GrooveEntity>

    @Query("SELECT * FROM saved_beats WHERE groove_id=:grooveId")
    fun getSavedBeatsForGroove(grooveId: Int): List<SavedBeatEntity>

    @Query("SELECT * FROM grooves WHERE id=:grooveId")
    fun findGrooveWithId(grooveId: Int): GrooveEntity

    @Query("DELETE FROM saved_beats WHERE groove_id=:grooveId")
    fun deleteSavedBeatsForGrooveId(grooveId: Int)

    @Insert
    fun insertGroove(grooveEntity: GrooveEntity): Long

    @Insert
    fun insertSavedBeats(vararg savedBeatEntity: SavedBeatEntity)

    @Delete
    fun deleteGroove(grooveEntity: GrooveEntity)

    @Delete
    fun deleteSavedBeats(vararg savedBeatEntity: SavedBeatEntity)
}